/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.theme.components')
        .controller('MsgCenterCtrl', MsgCenterCtrl);

    /** @ngInject */
    function MsgCenterCtrl($scope, $sce, $window, $http, $rootScope) {
        var web3 = new Web3(new Web3.providers.HttpProvider('http://18.209.231.241:8545'));
        // var web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:7545'));

        web3.eth.getAccounts(function (err, accs) {
            if (err != null) {
                alert("Error fetching accounts");
            }
            console.log("accounts = ", accs);
            accs.map(function(acc){
                console.log(acc + " balances = ",  web3.fromWei(web3.eth.getBalance(acc).toNumber()));
            });
            $rootScope.balance = web3.fromWei(web3.eth.getBalance(accs[2]).toNumber())
            $scope.accounts = accs;
        })

        $http.get('truffle/build/contracts/Greeter.json').then((function (res) {
            var jsonData = res.data;
            console.log("Greeter = ", jsonData);
            var abi = jsonData.abi;
            var address = jsonData.networks['5777'].address;
            var contract = web3.eth.contract(abi);
            var instance = contract.at(address);

            instance.greet(function (err, res) {
                console.log("res = ", res);
            });
        }));
        $http.get('truffle/build/contracts/FoneHome.json').then((function (res) {
            var jsonData = res.data;
            console.log("FoneHome = ", jsonData);
            var abi = jsonData.abi;
            var address = jsonData.networks['5777'].address;
            var contract = web3.eth.contract(abi);
            var instance = contract.at(address);
            $scope.instance = instance;
            // var newJobEvent = instance.PostJob();
            var takeJobEvent = instance.TakeJob();

            // newJobEvent.watch(function (err, res) {
            //     if (!err) {
            //         console.log("newJobEvent res = ", res);
            //     } else {
            //         console.log("err = ", err);
            //     }
            // });

            takeJobEvent.watch(function (err, res) {
                if (!err) {
                    console.log("takeJobEvent res = ", res);
                } else {
                    console.log("err = ", err);
                }
            });

            instance.InitEscrow().watch(function (err, res) {
                if (!err) {
                    console.log("InitEscrow res = ", res);
                } else {
                    console.log("err = ", err);
                }
            });

            instance.ReleaseEscrow().watch(function (err, res) {
                if (!err) {
                    console.log("ReleaseEscrow res = ", res);
                    $rootScope.balance = web3.fromWei(web3.eth.getBalance(res.args.receiver).toNumber());
                    $scope.$apply();
                    $rootScope.$apply();
                    console.log('BALANCE FUCKER', $rootScope.balance)
                } else {
                    console.log("err = ", err);
                }
            });
            var start = 0;
            instance.getAvailableJobsLength(function (err, res) {
                var jobsLength = res.toNumber();
                console.log("start = " + start + " res = ", jobsLength);
                for (var i = 0; i < jobsLength; i++) {
                    instance.getAvailableJob(i, function (err, res) {
                        //console.log("job = ", res);
                    });
                }
            });
        }));
        $scope.users = {
            0: {
                name: 'Vlad',
            },
            1: {
                name: 'Kostya',
            },
            2: {
                name: 'Andrey',
            },
            3: {
                name: 'Nasta',
            }
        };

        $scope.notifications = [
            {
                userId: 0,
                template: '&name posted a new article.',
                time: '1 min ago'
            },
            {
                userId: 1,
                template: '&name changed his contact information.',
                time: '2 hrs ago'
            },
            {
                image: 'assets/img/shopping-cart.svg',
                template: 'New orders received.',
                time: '5 hrs ago'
            },
            {
                userId: 2,
                template: '&name replied to your comment.',
                time: '1 day ago'
            },
            {
                userId: 3,
                template: 'Today is &name\'s birthday.',
                time: '2 days ago'
            },
            {
                image: 'assets/img/comments.svg',
                template: 'New comments on your post.',
                time: '3 days ago'
            },
            {
                userId: 1,
                template: '&name invited you to join the event.',
                time: '1 week ago'
            }
        ];

        $scope.messages = [
            {
                userId: 3,
                text: 'After you get up and running, you can place Font Awesome icons just about...',
                time: '1 min ago'
            },
            {
                userId: 0,
                text: 'You asked, Font Awesome delivers with 40 shiny new icons in version 4.2.',
                time: '2 hrs ago'
            },
            {
                userId: 1,
                text: 'Want to request new icons? Here\'s how. Need vectors or want to use on the...',
                time: '10 hrs ago'
            },
            {
                userId: 2,
                text: 'Explore your passions and discover new ones by getting involved. Stretch your...',
                time: '1 day ago'
            },
            {
                userId: 3,
                text: 'Get to know who we are - from the inside out. From our history and culture, to the...',
                time: '1 day ago'
            },
            {
                userId: 1,
                text: 'Need some support to reach your goals? Apply for scholarships across a variety of...',
                time: '2 days ago'
            },
            {
                userId: 0,
                text: 'Wrap the dropdown\'s trigger and the dropdown menu within .dropdown, or...',
                time: '1 week ago'
            }
        ];

        $scope.getMessage = function (msg) {
            var text = msg.template;
            if (msg.userId || msg.userId === 0) {
                text = text.replace('&name', '<strong>' + $scope.users[msg.userId].name + '</strong>');
            }
            return $sce.trustAsHtml(text);
        };

        $scope.takeJob = function () {
            console.log('take job butter');

            $scope.instance.takeJob(
                0,
                {
                    from: $scope.accounts[0],
                    gas: 4000000
                },
                function (err, res) {
                    console.log("taking job, res = ", res);
                });
        }

        $scope.completeJob = function () {
            console.log('completeJob job butter');

            $scope.instance.completeJob(
                1,
                {
                    from: $scope.accounts[1],
                    gas: 4000000
                },
                function (err, res) {
                    console.log("completeJob job, res = ", res);
                });
        }

        $scope.truffleButter = function () {
            console.log('juicy butter');

            $scope.instance.createJob(
                0,
                0,
                "fuck js",
                0,
                "js is shit, fuck it so bad",
                "43.6598",
                "79.3886",
                {
                    from: $scope.accounts[0],
                    gas: 4000000,
                    value: web3.toWei(5, "ether"),
                },
                function (err, res) {
                    console.log("creating job, res = ", res);
                });
        }

        $scope.directiveLoaded = function () {
            console.log('page loaded');
        }
    }
})();