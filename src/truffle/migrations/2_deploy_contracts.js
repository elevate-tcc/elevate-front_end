var Greeter = artifacts.require("./Greeter.sol");
var FoneHome = artifacts.require("./FoneHome.sol");

module.exports = function(deployer) {
  deployer.deploy(Greeter, "holla");
  deployer.deploy(FoneHome);
};
