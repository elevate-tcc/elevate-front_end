/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.dashboard')
    .controller('BlurFeedCtrl', BlurFeedCtrl);

  /** @ngInject */
  function BlurFeedCtrl($scope, $http, $rootScope) {
    $scope.feeds = [
      {
        type: 'text-message',
        author: 'City',
        surname: 'of Toronto',
        header: 'Posted new message',
        text: 'Posted a new job',
        time: 'Today 11:55 pm',
        ago: '25 minutes ago',
        expanded: false,
      }, {
        type: 'text-message',
        author: 'Samsung',
        surname: '',
        header: 'Posted new message',
        text: 'Posted a new job',
        time: 'Today 11:55 pm',
        ago: '25 minutes ago',
        expanded: false,
      }, {
        type: 'text-message',
        author: 'Nest',
        surname: '',
        header: 'Posted new message',
        text: 'Posted a new job',
        time: 'Today 11:55 pm',
        ago: '25 minutes ago',
        expanded: false,
      }, {
        type: 'text-message',
        author: 'City',
        surname: 'of Toronto',
        header: 'Posted new message',
        text: 'Posted a new job',
        time: 'Today 11:55 pm',
        ago: '25 minutes ago',
        expanded: false,
      }, {
        type: 'text-message',
        author: 'iRobot',
        surname: '',
        header: 'Posted new message',
        text: 'Posted a new job',
        time: 'Today 11:55 pm',
        ago: '25 minutes ago',
        expanded: false,
      }, {
        type: 'text-message',
        author: 'LG',
        surname: '',
        header: 'Posted new message',
        text: 'Posted a new job',
        time: 'Today 11:55 pm',
        ago: '25 minutes ago',
        expanded: false,
      },
    ];
    $rootScope.feed = [];
    for(var x=0; x<$scope.feeds.length;x++){
      $rootScope.feed.push($scope.feeds[x]);
    }
    var owner_name = ['Ecobee'];
    var device = ['Thermostat', 'Garbage can'];
    var type = ['Repair', 'Pick up'];
    $http.get('truffle/build/contracts/FoneHome.json').then(function (res) {
      var web3 = new Web3(new Web3.providers.HttpProvider('http://18.209.231.241:8545'));
      var jsonData = res.data;
      var abi = jsonData.abi;
      var address = jsonData.networks['5777'].address;
      var contract = web3.eth.contract(abi);
      var instance = contract.at(address);

      var newJobEvent = instance.PostJob();

      newJobEvent.watch(function (err, newjob) {
          if (!err) {
            var jobId = newjob.args._jobId.toNumber();
            console.log("newJobEvent.watch = " + newjob + " sdasdad = " + jobId);
            instance.getAvailableJob(jobId, function (err, res) {
              $rootScope.feed.push({
                type: 'text-message',
                author: owner_name[res[5]],
                surname: "",
                header: "Posted a new message",
                text: "Posted a new job",
                time: res[6],
                ago: '3 minutes ago',
                expanded: false,
              })
              $scope.$apply();
            });
          } else {
              console.log("err = ", err);
          }
      });

      instance.getAvailableJobsLength(function (err, res) {
        var jobsLength = res.toNumber();
        console.log("length res = ", jobsLength);
        for (var i = 0; i < jobsLength; i++) {
          instance.getAvailableJob(i, function (err, res) {
            $rootScope.feed.push({
              type: 'text-message',
              author: owner_name[res[5]],
              surname: "",
              header: "Posted a new message",
              text: "Posted a new job",
              time: res[6],
              ago: '3 minutes ago',
              expanded: false,
            })
            console.log("asdasdasdasdasdjob = ", res);
            $scope.$apply();
          });
        }
      })
    });

    

    $scope.expandMessage = function (message) {
      message.expanded = !message.expanded;
    }
  }
})();