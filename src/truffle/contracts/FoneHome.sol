pragma solidity ^0.4.18;

contract FoneHome {
	enum JobState {Open, InProgress, Completed, Rejected}
	enum JobType {Repair, RMA}

	uint public jobsAvailablLength;
	mapping (uint => Job) public jobsAvailable;
	mapping (address => Job[]) public jobsAccepted;
	mapping (address => Job[]) public jobsGiven;
	mapping (address => uint) public balances;

	struct Job {
		uint jobId;
		JobState jobState;
		uint jobType;
		address owner;
		uint ownerName;
		uint bounty;
		string name;
		uint device;
		string description;
		string latitude;
		string longitude;
	}

	event PostJob(
		uint _jobId,
		JobState _jobState,
		uint _jobType,
		address _owner,
		uint _ownerName,
		uint _bounty,
		string _name,
		uint _device,
		string _description,
		string _latitude,
		string _longitude
	);

	event TakeJob(
		uint _jobId,
		JobState _jobState,
		uint _jobType,
		address _owner,
		uint _ownerName,
		uint _bounty,
		string _name,
		uint _device,
		string _description,
		string _latitude,
		string _longitude
	);

	event CompleteJob(
		uint _jobId,
		JobState _jobState,
		uint _jobType,
		address _owner,
		uint _ownerName,
		uint _bounty,
		string _name,
		uint _device,
		string _description,
		string _latitude,
		string _longitude
	);

	event InitEscrow(address giver, uint value);
	event ReleaseEscrow(address giver, address receiver, uint value);

	function createJob(
		uint jobType,
		uint _ownerName,
		string _name,
		uint _device,
		string _description,
		string _latitude,
		string _longitude
	) public payable returns(uint) {
		uint _jobId = jobsAvailablLength++;
		Job memory newJob = Job({
			jobId: _jobId,
			jobState: JobState.Open,
			jobType: jobType,
			owner: msg.sender,
			ownerName: _ownerName,
			bounty: msg.value,
			name: _name,
			device: _device,
			description: _description,
			latitude: _latitude,
			longitude: _longitude
		});

		jobsAvailable[_jobId] = newJob;
		jobsGiven[msg.sender].push(newJob);
        balances[msg.sender] += msg.value;
		emit InitEscrow(msg.sender, msg.value);
		emit PostJob(
			_jobId,
			JobState.Open,
			jobType,
			msg.sender,
			_ownerName,
			msg.value,
			_name,
			_device,
			_description,
			_latitude,
			_longitude
		);
		return _jobId;
	}

	function takeJob(
		uint jobId
	) public {
		Job storage job = jobsAvailable[jobId];
		job.jobState = JobState.InProgress;
		jobsAccepted[msg.sender].push(job);

		emit TakeJob(
			jobsAvailable[jobId].jobId,
			jobsAvailable[jobId].jobState,
			jobsAvailable[jobId].jobType,
			jobsAvailable[jobId].owner,
			jobsAvailable[jobId].ownerName,
			jobsAvailable[jobId].bounty,
			jobsAvailable[jobId].name,
			jobsAvailable[jobId].device,
			jobsAvailable[jobId].description,
			jobsAvailable[jobId].latitude,
			jobsAvailable[jobId].longitude
		);
	}

	function completeJob(
		uint jobId
	) public {
		Job storage job = jobsAvailable[jobId];
		job.jobState = JobState.Completed;
        balances[job.owner] -= job.bounty;
		msg.sender.transfer(job.bounty);
		emit ReleaseEscrow(job.owner, msg.sender, job.bounty);
		emit CompleteJob(
			job.jobId,
			job.jobState,
			job.jobType,
			job.owner,
			job.ownerName,
			job.bounty,
			job.name,
			job.device,
			job.description,
			job.latitude,
			job.longitude
		);
	}
	function getAvailableJobsLength() public view returns(uint) {
		return jobsAvailablLength;
	}

	function getAvailableJobOwner(uint jobId) public view returns(
		uint, address, uint
	) {
		Job storage job = jobsAvailable[jobId];
		return(
			job.jobId,
			job.owner,
			job.ownerName
		);
	}

	function getAvailableJobGeo(uint jobId) public view returns(
		uint, string, string
	) {
		Job storage job = jobsAvailable[jobId];
		return(
			job.jobId,
			job.latitude,
			job.longitude
		);
	}

	function getAvailableJob(uint jobId) public view returns(
		uint,
		JobState,
		uint,
		uint,
		string,
		uint,
		string
	) {
		Job storage job = jobsAvailable[jobId];
		return(
			job.jobId,
			job.jobState,
			job.jobType,
			job.bounty,
			job.name,
			job.device,
			job.description
		);
	}

	function getAvailableJobInfo(uint jobId) public view returns(
		uint,
		string,
		string,
		JobState,
		uint,
		uint,
		string,
		uint,
		string
	) {
		Job storage job = jobsAvailable[jobId];
		return(
			job.jobId,
			job.latitude,
			job.longitude,
			job.jobState,
			job.jobType,
			job.bounty,
			job.name,
			job.device,
			job.description
		);
	}
}
